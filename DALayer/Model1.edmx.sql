
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/19/2018 16:18:33
-- Generated from EDMX file: F:\An3Sem2\SD\Peter\assig222\DALayer\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HW2];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Assignment_Laboratory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Assignments] DROP CONSTRAINT [FK_Assignment_Laboratory];
GO
IF OBJECT_ID(N'[dbo].[FK_Attendance_Laboratory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attendances] DROP CONSTRAINT [FK_Attendance_Laboratory];
GO
IF OBJECT_ID(N'[dbo].[FK_Attendance_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attendances] DROP CONSTRAINT [FK_Attendance_User];
GO
IF OBJECT_ID(N'[dbo].[FK_Submission_Assignment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Submissions] DROP CONSTRAINT [FK_Submission_Assignment];
GO
IF OBJECT_ID(N'[dbo].[FK_Submission_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Submissions] DROP CONSTRAINT [FK_Submission_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Assignments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Assignments];
GO
IF OBJECT_ID(N'[dbo].[Attendances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Attendances];
GO
IF OBJECT_ID(N'[dbo].[Laboratories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Laboratories];
GO
IF OBJECT_ID(N'[dbo].[Submissions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Submissions];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Assignments'
CREATE TABLE [dbo].[Assignments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Lab_Id] int  NULL,
    [Name] nvarchar(50)  NULL,
    [Deadline] datetime  NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'Attendances'
CREATE TABLE [dbo].[Attendances] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LabId] int  NOT NULL,
    [StudentId] int  NOT NULL
);
GO

-- Creating table 'Laboratories'
CREATE TABLE [dbo].[Laboratories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(50)  NULL,
    [Curricula] nvarchar(50)  NULL,
    [Date] datetime  NULL,
    [Number] int  NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'Submissions'
CREATE TABLE [dbo].[Submissions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Assign_Id] int  NOT NULL,
    [Student_Id] int  NOT NULL,
    [GitLink] varchar(50)  NOT NULL,
    [Note] varchar(50)  NOT NULL,
    [Grade] float  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(50)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [Password] nvarchar(50)  NULL,
    [Token] nvarchar(50)  NULL,
    [GroupNr] nvarchar(50)  NOT NULL,
    [FullName] nvarchar(50)  NULL,
    [Hobby] nvarchar(50)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Assignments'
ALTER TABLE [dbo].[Assignments]
ADD CONSTRAINT [PK_Assignments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [PK_Attendances]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Laboratories'
ALTER TABLE [dbo].[Laboratories]
ADD CONSTRAINT [PK_Laboratories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Submissions'
ALTER TABLE [dbo].[Submissions]
ADD CONSTRAINT [PK_Submissions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Lab_Id] in table 'Assignments'
ALTER TABLE [dbo].[Assignments]
ADD CONSTRAINT [FK_Assignment_Laboratory]
    FOREIGN KEY ([Lab_Id])
    REFERENCES [dbo].[Laboratories]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Assignment_Laboratory'
CREATE INDEX [IX_FK_Assignment_Laboratory]
ON [dbo].[Assignments]
    ([Lab_Id]);
GO

-- Creating foreign key on [Assign_Id] in table 'Submissions'
ALTER TABLE [dbo].[Submissions]
ADD CONSTRAINT [FK_Submission_Assignment]
    FOREIGN KEY ([Assign_Id])
    REFERENCES [dbo].[Assignments]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Submission_Assignment'
CREATE INDEX [IX_FK_Submission_Assignment]
ON [dbo].[Submissions]
    ([Assign_Id]);
GO

-- Creating foreign key on [LabId] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [FK_Attendance_Laboratory]
    FOREIGN KEY ([LabId])
    REFERENCES [dbo].[Laboratories]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Attendance_Laboratory'
CREATE INDEX [IX_FK_Attendance_Laboratory]
ON [dbo].[Attendances]
    ([LabId]);
GO

-- Creating foreign key on [StudentId] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [FK_Attendance_User]
    FOREIGN KEY ([StudentId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Attendance_User'
CREATE INDEX [IX_FK_Attendance_User]
ON [dbo].[Attendances]
    ([StudentId]);
GO

-- Creating foreign key on [Student_Id] in table 'Submissions'
ALTER TABLE [dbo].[Submissions]
ADD CONSTRAINT [FK_Submission_User]
    FOREIGN KEY ([Student_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Submission_User'
CREATE INDEX [IX_FK_Submission_User]
ON [dbo].[Submissions]
    ([Student_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------