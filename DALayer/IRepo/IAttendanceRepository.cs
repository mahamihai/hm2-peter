﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALayer.IRepo
{
    public interface IAttendanceRepository
    {
        void AddAttendance(Attendance attendance);

        void DeleteAttendance(int Id);
        List<Attendance> GetById();
        void UpdateAttendance(Attendance attendance);
    }
}
