﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALayer.IRepo
{
    public interface IUserRepository
    {
        void AddUser(User user);

        void DeleteUser(int Id);

        void UpdateUser(User user);
        List<User> getStudents();
        User CheckUser(string uss, string pass);
        void CreateUser(string type, string token, string groupNr, string name, string hobby);

        void SignUp(string token, string email, string pass);
    }
}
