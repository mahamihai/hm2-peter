﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALayer.IRepo
{
    public interface ILaboratoryRepository
    {
        void AddLab(Laboratory lab);

        void DeleteLaboratory(int id);
        List<Laboratory> getLabs();
        Laboratory GetById(int id);
        void UpdateLaboratory(Laboratory lab);
    }
}
