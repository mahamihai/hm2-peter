﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALayer.IRepo
{
    public interface IAssignmentRepository
    {
        void AddAssig(Assignment assi);

        void DeleteAssignment(int id);
        List<Assignment> GetAll();
        void UpdateAssignment(Assignment assi);
    }
}
