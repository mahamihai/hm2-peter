﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALayer.IRepo;

namespace DALayer.Repo
{
    public class LaboratoryRepository : ILaboratoryRepository
    {
        public HW2Entities context;

        public LaboratoryRepository()
        {
            context = new HW2Entities();
        }

        public void AddLab(Laboratory lab)
        {
            context.Set<Laboratory>().Add(lab);
            context.SaveChanges();
        }

        public Laboratory GetById(int id)
        {
            var received = context.Set<Laboratory>().Find(id);
            return received;
        }
        public void DeleteLaboratory(int id)
        {
            var toBeDeleted = context.Set<Laboratory>().Find(id);
            context.Set<Laboratory>().Remove(toBeDeleted);
            context.SaveChanges();
        }
        public List<Laboratory> getLabs()
        {
            return this.context.Set<Laboratory>().ToList();
        }

        public void UpdateLaboratory(Laboratory lab)
        {
            var toBeUpdated = context.Set<Laboratory>().Find(lab.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(lab);
            context.SaveChanges();
        }
    }
}
