﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALayer.IRepo;

namespace DALayer.Repo
{
    public class UserRepository : IUserRepository
    {
        public HW2Entities context;

        public UserRepository()
        {
            context = new HW2Entities();
        }

        public void AddUser(User user)
        {
            context.Set<User>().Add(user);
            context.SaveChanges();
        }

        public List<User> getStudents()
        {
            var students = context.Set<User>().Where(x => x.Type.Equals("Student")).ToList();
            return students;
        }
        public void DeleteUser(int Id)
        {
            var toBeDeleted = context.Set<User>().Find(Id);
            context.Set<User>().Remove(toBeDeleted);
            context.SaveChanges();
        }

        public void UpdateUser(User user)
        {
            var toBeUpdated = context.Set<User>().Find(user.Id);
          //  context.Entry(toBeUpdated).CurrentValues.SetValues(user);
            toBeUpdated.Email = user.Email;
            toBeUpdated.Hobby = user.Hobby;
            toBeUpdated.GroupNr = user.GroupNr;
            context.SaveChanges();
        }

        public User CheckUser(string uss, string pass)
        {
            var check = context.Set<User>().FirstOrDefault(x=>x.Password.Equals(pass)&&x.Email.Equals(uss));
            
            if(check == null)
            {
                return null;
            }
            else
            {
                return check;
            }

        }

        public void CreateUser(string type, string token,string groupNr, string email, string hobby)
        {
            User user = new User
            {
                Type = type,
                Token = token,
                GroupNr = groupNr,
                Email = email,
                Hobby = hobby
            };

            context.Set<User>().Add(user);
            context.SaveChanges();
        }

        public void SignUp(string token, string name, string pass)
        {
            var toBeUpdated = context.Set<User>().Where(x=>x.Token.Equals(token)).FirstOrDefault();
            if (toBeUpdated != null)
            {
                toBeUpdated.FullName = name;
                toBeUpdated.Password = pass;
                toBeUpdated.Token = "";
                context.SaveChanges();
            }
        }
    }
}
