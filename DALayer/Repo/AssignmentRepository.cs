﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALayer.IRepo;

namespace DALayer.Repo
{
    public class AssignmentRepository : IAssignmentRepository
    {
        public HW2Entities context;

        public AssignmentRepository()
        {
            context = new HW2Entities();
        }

        public void AddAssig(Assignment assi)
        {
            context.Set<Assignment>().Add(assi);
            context.SaveChanges();
        }

        public void DeleteAssignment(int id)
        {
            var toBeDeleted = context.Set<Assignment>().Find(id);
            context.Set<Assignment>().Remove(toBeDeleted);
            context.SaveChanges();
        }

        public void UpdateAssignment(Assignment assi)
        {
            var toBeUpdated = context.Set<Assignment>().Find(assi.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(assi);
            context.SaveChanges();
        }
        public List<Assignment> GetAll()
        {
            List<Assignment> assignments = new List<Assignment>();
            assignments = context.Set<Assignment>().ToList<Assignment>();
            return assignments;
        }
    }
}
