﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALayer.IRepo;

namespace DALayer.Repo
{
    public class AttendanceRepository : IAttendanceRepository
    {
        public HW2Entities context;

        public AttendanceRepository()
        {
            context = new HW2Entities();
        }

        public void AddAttendance(Attendance attendance)
        {
            context.Set<Attendance>().Add(attendance);
            context.SaveChanges();
        }

        public void DeleteAttendance(int Id)
        {
            var toBeDeleted = context.Set<Attendance>().Find(Id);
            context.Set<Attendance>().Remove(toBeDeleted);
            context.SaveChanges();
        }
        public List<Attendance> GetById()
        {
            var received = context.Set<Attendance>().ToList<Attendance>();
            return received;
        }
        public void UpdateAttendance(Attendance attendance)
        {
            var toBeUpdated = context.Set<Attendance>().Find(attendance.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(attendance);
            context.SaveChanges();
        }
    }
}
