﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assig222.Models
{
    public class LaboratoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Curricula { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Number { get; set; }
        public string Description { get; set; }
    }
}