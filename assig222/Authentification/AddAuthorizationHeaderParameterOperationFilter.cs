﻿using System.Collections.Generic;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace assig222.Authentification
{
    public class AddAuthorizationHeaderParameterOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
            {
                operation.parameters = new List<Parameter>();
            }
            operation.parameters.Add(new Parameter()
            {
                name = "Authorization",
                @in = "header",
                description = "admin,admin or student,student or both,both [user,password].Input format <user,pass>",
                required = false,
                type = "string"
            });

        }
    }
}