﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assig222.Models
{
    public class SubmissionModel
    {
        public int Id { get; set; }
        public int Assign_Id { get; set; }
        public int Student_Id { get; set; }
        public string GitLink { get; set; }
        public string Note { get; set; }
        public double Grade { get; set; }
    }
}