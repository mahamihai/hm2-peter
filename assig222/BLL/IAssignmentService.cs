﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assig222.Models;

namespace assig222.BLL
{
    interface IAssignmentService
    {
        void AddAssignment(AssignmentModel assi);

        void DeleteAssignment(int id);
        List<AssignmentModel> GetAssi();
        void UpdateAssignment(AssignmentModel assi);
    }
}
