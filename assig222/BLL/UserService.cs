﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DALayer.IRepo;
using DALayer.Repo;
using assig222.Models;
using assig222.Controllers.Mappers;
using HM2.BLL.Services;

namespace assig222.BLL
{
    public class UserService : IUserService
    {
        private static Random random = new Random();

        public static string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private IUserRepository userRepo;

        public UserService()
        {
            userRepo = new UserRepository();
        }

        public void AddUser(UserModel user)
        {
            UserMapper mapper = new UserMapper();
            var toBeAdded = mapper.UserMaper(user);
            userRepo.AddUser(toBeAdded);
        }

        public void DeleteUser(int Id)
        {
            userRepo.DeleteUser(Id);
        }

        public void UpdateUser(UserModel user)
        {
            UserMapper mapper = new UserMapper();
            var toBeUpdated = mapper.UserMaper(user);
            userRepo.UpdateUser(toBeUpdated);
        }

        public List<UserModel> getStudents()
        {
           var students= this.userRepo.getStudents();
            UserMapper mapper = new UserMapper();
            var converted = students.Select(x => mapper.UserMaper(x)).ToList();
            return converted;
        }

        public UserModel CheckUser(string uss, string pass)
        {
             var rez = userRepo.CheckUser(uss, pass);
            UserMapper usr=new UserMapper();
            if (rez != null)
            {
                return usr.UserMaper(rez);
            }
            else
            {
                return null;
            }

        }

        public string CreateUser(string type, string groupNr, string email, string hobby)
        {
            string token = RandomString();
            userRepo.CreateUser(type, token, groupNr, email, hobby);
            (new EmailFactory()).sendEmail(email,token);
            return token;
        }

        public void SignUp(string token, string name, string pass)
        {
            userRepo.SignUp(token, name, pass);
        }
        
    }
}