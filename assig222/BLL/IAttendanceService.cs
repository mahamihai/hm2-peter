﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assig222.Models;

namespace assig222.BLL
{
    public interface IAttendanceService
    {
        void AddAttendance(AttendanceModel attendance);

        void DeleteAttendance(int Id);
        List<AttendanceModel> GetById();
        void UpdateAttendance(AttendanceModel attendance);
    }
}
