﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DALayer.IRepo;
using DALayer.Repo;
using assig222.Models;
using assig222.Controllers.Mappers;
using assig222.Controllers.APIModel;
using DALayer;

namespace assig222.BLL
{
    public class AttendanceService : IAttendanceService
    {
        private IAttendanceRepository attendanceRepo;

        public AttendanceService()
        {
            attendanceRepo = new AttendanceRepository();
        }

        public List<AttendanceModel> GetById()
        {
            var received = attendanceRepo.GetById();
            AttendanceMapper mapper = new AttendanceMapper();

            List<AttendanceModel> toBeReturned = new List<AttendanceModel>();

            foreach (Attendance att in received)
            {
                toBeReturned.Add(mapper.AttendMapper(att));
            }

            return toBeReturned;
        }

        public void AddAttendance(AttendanceModel attendance)
        {
            AttendanceMapper mapper = new AttendanceMapper();
            var toBeAdded = mapper.AttendMapper(attendance);
            attendanceRepo.AddAttendance(toBeAdded);
        }

        public void DeleteAttendance(int Id)
        {
            attendanceRepo.DeleteAttendance(Id);
        }

        public void UpdateAttendance(AttendanceModel att)
        {
            AttendanceMapper mapper = new AttendanceMapper();
            var toBeUpdated = mapper.AttendMapper(att);
            attendanceRepo.UpdateAttendance(toBeUpdated);
        }
    }
}