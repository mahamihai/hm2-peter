﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assig222.Models;

namespace assig222.BLL
{
    public interface ILaboratoryService
    {
        void AddLaboratory(LaboratoryModel lab);

        void DeleteLaboratory(int id);
        LaboratoryModel GetById(int Id);
        List<LaboratoryModel> GetLabs();
        void UpdateLaboratory(LaboratoryModel lab);
    }
}
