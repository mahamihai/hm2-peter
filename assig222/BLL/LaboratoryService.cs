﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DALayer.IRepo;
using DALayer.Repo;
using assig222.Models;
using assig222.Controllers.Mappers;
using assig222.Controllers.APIModel;


namespace assig222.BLL
{
    public class LaboratoryService : ILaboratoryService
    {
        private ILaboratoryRepository labRepo;

        public LaboratoryService()
        {
            labRepo = new LaboratoryRepository();
        }

        public void AddLaboratory(LaboratoryModel lab)
        {
            LaboratoryMapper mapper = new LaboratoryMapper();
            var toBeAdded = mapper.LabMapper(lab);
            labRepo.AddLab(toBeAdded);
        }

        public void DeleteLaboratory(int id)
        {
            labRepo.DeleteLaboratory(id);
        }
        public List<LaboratoryModel> GetLabs()
        {
            LaboratoryMapper mapper = new LaboratoryMapper();
            var labs = this.labRepo.getLabs();
            var convertedLabs = labs.Select(x => mapper.LabMapper(x)).ToList();
            return convertedLabs;
        }

        public LaboratoryModel GetById(int Id)
        {
            var received = labRepo.GetById(Id);
            LaboratoryMapper mapper = new LaboratoryMapper();
            LaboratoryModel toBeReturned = mapper.LabMapper(received);
            return toBeReturned;
        }

        public void UpdateLaboratory(LaboratoryModel lab)
        {
            LaboratoryMapper mapper = new LaboratoryMapper();
            var toBeUpdated = mapper.LabMapper(lab);
            labRepo.UpdateLaboratory(toBeUpdated);
        }

    }
}