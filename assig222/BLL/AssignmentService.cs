﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DALayer.IRepo;
using DALayer.Repo;
using assig222.Models;
using assig222.Controllers.Mappers;

namespace assig222.BLL
{
    public class AssignmentService : IAssignmentService
    {
        private IAssignmentRepository assiRepo;

        public AssignmentService()
        {
            assiRepo = new AssignmentRepository();
        }

       public  List<AssignmentModel> GetAssi()
        {
            AssignmentMapper mapper = new AssignmentMapper();

            var all = this.assiRepo.GetAll();
            var converted = all.Select(x => mapper.AssigMapper(x)).ToList();
            return converted;
        }

        public void AddAssignment(AssignmentModel assi)
        {
            AssignmentMapper mapper = new AssignmentMapper();
            var toBeAdded = mapper.AssigMapper(assi);
            assiRepo.AddAssig(toBeAdded);
        }

        public void DeleteAssignment(int id)
        {
            assiRepo.DeleteAssignment(id);
        }

        public void UpdateAssignment(AssignmentModel assi)
        {
            AssignmentMapper mapper = new AssignmentMapper();
            var toBeUpdated = mapper.AssigMapper(assi);
            assiRepo.UpdateAssignment(toBeUpdated);
        }

    }
}