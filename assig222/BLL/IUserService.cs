﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assig222.Models;

namespace assig222.BLL
{
    interface IUserService
    {
        void AddUser(UserModel user);

        void DeleteUser(int Id);

        void UpdateUser(UserModel user);
        List<UserModel> getStudents();
        UserModel CheckUser(string uss, string pass);
        string CreateUser(string type, string groupNr, string name, string hobby);

        void SignUp(string token, string email, string pass);
    }
}
