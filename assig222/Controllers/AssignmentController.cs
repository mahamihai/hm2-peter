﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using assig222.Models;
using assig222.BLL;
using assig222.Controllers.APIModel;
using assig222.Controllers.Mappers;

namespace assig222.Controllers
{
    public class AssignmentController : ApiController
    {
        private AssignmentService assignment = new AssignmentService();
        AssignmentMapper assiMapper = new AssignmentMapper();

        [HttpGet]
        [Route("Assignments/")]
        [Authorize(Roles = "Admin")]
        public List<AssignmentModel> Get()
        {
            List<AssignmentModel> assignments = new List<AssignmentModel>();

            assignments = this.assignment.GetAssi();

            return assignments;
        }
        [HttpGet]
        [Route("Assignments/{labId}")]
        [Authorize(Roles = "Admin, Student")]
        public List<AssignmentModel> GetById(int labId)
        {
            List<AssignmentModel> assignments = new List<AssignmentModel>();

            assignments = this.assignment.GetAssi();
            var filtered = assignments.Where(x => x.Lab_Id.Equals(labId)).ToList();

            return filtered;
        }

        // POST: api/Assignment
        [HttpPost]
        [Route("Assignments/")]
        [Authorize(Roles = "Admin")]
        public void Post([FromBody]AssignmentAPI assi)
        {
            var converted = assiMapper.AssignmentMaper(assi);
            this.assignment.AddAssignment(converted);
        }

        [HttpPut]
        [Route("Assignments/")]
        [Authorize(Roles = "Admin")]
        // PUT: api/Assignment/5
        public void Put( [FromBody]AssignmentModel assi)
        {
            this.assignment.UpdateAssignment(assi);
        }
        [HttpDelete]
        [Route("Assignments/{id}")]
        [Authorize(Roles = "Admin")]
        // DELETE: api/Assignment/5
        public void Delete(int id)
        {
            this.assignment.DeleteAssignment(id);
        }
    }
}
