﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using assig222.Models;
using assig222.BLL;
using assig222.Controllers.APIModel;
using assig222.Controllers.Mappers;

namespace assig222.Controllers
{
    public class LaboratoryController : ApiController
    {
        private LaboratoryService lab = new LaboratoryService();
        LaboratoryMapper labMapper = new LaboratoryMapper();

        // GET: api/Laboratory
        [Route("Laboratory/")]
        [HttpGet]
        [Authorize(Roles = "Admin, Student")]
        public List<LaboratoryModel> getLabs()
        {
            var all = this.lab.GetLabs();
            return all;
        }
        [Route("Laboratory/")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        // POST: api/Laboratory
        public void Post([FromBody]LaboratoryAPI labAPI)
        {
            var converted = labMapper.LaboratoryMaper(labAPI);
            this.lab.AddLaboratory(converted);
        }

        [Route("Laboratory/")]
        [HttpPut]
        [Authorize(Roles = "Admin")]
        public void Put( [FromBody]LaboratoryModel labModel)
        {
            this.lab.UpdateLaboratory(labModel);
        }

        [Route("Laboratory/")]
        [HttpDelete]
        [Authorize(Roles = "Admin")]
        public void Delete(int id)
        {
            this.lab.DeleteLaboratory(id);
        }
    }
}
