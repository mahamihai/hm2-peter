﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using assig222.Models;
using assig222.BLL;
using assig222.Controllers.APIModel;
using assig222.Controllers.Mappers;

namespace assig222.Controllers
{
    public class UserController : ApiController
    {
        //private IUserService _IUserService = new UserService();

        private UserService user = new UserService();
        UserMapper usermapper = new UserMapper();

        // GET: api/User
     

        // POST: api/User
        public void Post([FromBody]UserAPI user)
        {
            var converted = usermapper.UserMaper(user);
            this.user.AddUser(converted);

        }
        [Route("Users/")]
        [HttpGet]
        public List<UserModel> GetStudents()
        {
            var students = this.user.getStudents();
            return students;
        }
        [Route("Users/")]
        [HttpPut]
        public void Put([FromBody]UserModel user)
        {
            this.user.UpdateUser(user);
        }

        // DELETE: api/User/5
        [Route("Users/{Id}")]
        [HttpDelete]
        public void Delete(int Id)
        {
            this.user.DeleteUser(Id);
        }

        //LogIn
        [HttpGet]
        [Route("Users/login")]
        public UserModel LogIn(string uss,string pass)
        {
           var t= this.user.CheckUser(uss, pass);
            return t;
        }

        //CreateUsser
        [HttpPost]
        [Route("Users/")]
        public string CreateUser(string groupNr, string email, string hobby)
        {
            return this.user.CreateUser("Student", groupNr, email, hobby);
        }

        //SignUp
        [HttpPut]
        [Route("Users/{token}/{name}/{pass}")]
        public void SignUp(string token, string name, string pass)
        {
            this.user.SignUp(token, name, pass);
        }

    }
}
