﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using assig222.Models;
using DALayer;
using assig222.Controllers.APIModel;


namespace assig222.Controllers.Mappers
{
    public class AssignmentMapper
    {
        public AssignmentModel AssigMapper(Assignment assign)
        {
            return new AssignmentModel()
            {
                Id = assign.Id,
                Lab_Id = assign.Lab_Id,
                Name = assign.Name,
                Deadline = assign.Deadline,
                Description = assign.Description
            };
        }

        public Assignment AssigMapper(AssignmentModel assign)
        {
            return new Assignment()
            {
                Id = assign.Id,
                Lab_Id = assign.Lab_Id,
                Name = assign.Name,
                Deadline = assign.Deadline,
                Description = assign.Description
            };
        }

        public AssignmentModel AssignmentMaper(AssignmentAPI assi)
        {
            return new AssignmentModel
            {
                Lab_Id = assi.Lab_Id,
                Name = assi.Name,
                Deadline = assi.Deadline,
                Description = assi.Description
            };
        }
    }
}