﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using assig222.Models;
using DALayer;
using AutoMapper;

namespace assig222.Controllers.Mappers
{
    public class SubmissionMapper
    {
        public SubmissionModel SubMapper(Submission submission)
        {
            return new SubmissionModel()
            {
                Id = submission.Id,
                Assign_Id = submission.Assign_Id,
                Student_Id = submission.Student_Id,
                GitLink = submission.GitLink,
                Note = submission.Note,
                Grade = submission.Grade
            };
        }

        public Submission SubMapper(SubmissionModel submission)
        {
            return new Submission()
            {
                Id = submission.Id,
                Assign_Id = submission.Assign_Id,
                Student_Id = submission.Student_Id,
                GitLink = submission.GitLink,
                Note = submission.Note,
                Grade = submission.Grade
            };
        }
    }
}