﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using assig222.Models;
using DALayer;
using assig222.Controllers.APIModel;


namespace assig222.Controllers.Mappers
{
    public class LaboratoryMapper
    {
        public LaboratoryModel LabMapper(Laboratory lab)
        {
            return new LaboratoryModel()
            {
                Id = lab.Id,
                Title = lab.Title,
                Curricula = lab.Curricula,
                Date = lab.Date,
                Number = lab.Number,
                Description = lab.Description
            };
        }

        public Laboratory LabMapper(LaboratoryModel lab)
        {
            return new Laboratory()
            {
                Id = lab.Id,
                Title = lab.Title,
                Curricula = lab.Curricula,
                Date = lab.Date,
                Number = lab.Number,
                Description = lab.Description
            };
        }

        public LaboratoryModel LaboratoryMaper(LaboratoryAPI lab)
        {
            return new LaboratoryModel
            {
                Title = lab.Title,
                Curricula = lab.Curricula,
                Date = lab.Date,
                Number = lab.Number,
                Description = lab.Description
            };
        }
    }
}