﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using assig222.Models;
using DALayer;
using AutoMapper;
using assig222.Controllers.APIModel;

namespace assig222.Controllers.Mappers
{
    public class UserMapper
    {
        public UserModel UserMaper(User user)
        {
            return new UserModel
            {
                Id = user.Id,
                Type = user.Type,
                Email = user.Email,
                Password = user.Password,
                Token = user.Token,
                GroupNr = user.GroupNr,
                FullName = user.FullName,
                Hobby = user.Hobby
            };
        }

        public User UserMaper(UserModel user)
        {
            return new User
            {
                Id = user.Id,
                Type = user.Type,
                Email = user.Email,
                Password = user.Password,
                Token = user.Token,
                GroupNr = user.GroupNr,
                FullName = user.FullName,
                Hobby = user.Hobby
            };
        }

        public UserModel UserMaper(UserAPI user)
        {
            return new UserModel
            {
                Type = user.Type,
                Email = user.Email,
                Password = user.Password,
                Token = user.Token,
                GroupNr = user.GroupNr,
                FullName = user.FullName,
                Hobby = user.Hobby
            };
        }
    }
}