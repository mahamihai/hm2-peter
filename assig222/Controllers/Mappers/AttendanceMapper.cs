﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using assig222.Models;
using DALayer;
using assig222.Controllers.APIModel;


namespace assig222.Controllers.Mappers
{
    public class AttendanceMapper
    {
        public AttendanceModel AttendMapper(Attendance attend)
        {
            return new AttendanceModel()
            {
                Id = attend.Id,
                LabId = attend.LabId,
                StudentId = attend.StudentId
            };
        }

        public Attendance AttendMapper(AttendanceModel attend)
        {
            return new Attendance()
            {
                Id = attend.Id,
                LabId = attend.LabId,
                StudentId = attend.StudentId
            };
        }

        public AttendanceModel AttendanceMaper(AttendanceAPI att)
        {
            return new AttendanceModel
            {
                LabId = att.LabId,
                StudentId = att.StudentId
            };
        }
    }
}