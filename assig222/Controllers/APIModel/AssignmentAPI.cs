﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assig222.Controllers.APIModel
{
    public class AssignmentAPI
    {

     
        public Nullable<int> Lab_Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> Deadline { get; set; }
        public string Description { get; set; }
    }
}