﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using assig222.Models;
using assig222.BLL;
using assig222.Controllers.APIModel;
using assig222.Controllers.Mappers;


namespace assig222.Controllers
{
    public class AttendanceController : ApiController
    {

        private IAttendanceService att = new AttendanceService();
        AttendanceMapper attMapper = new AttendanceMapper();

        // GET: api/Attendance


        [HttpPost]
        [Route("Attendance/")]
        [Authorize(Roles = "Admin")]
        // POST: api/Attendance
        public void Post([FromBody]AttendanceAPI atte)
        {
            var converted = attMapper.AttendanceMaper(atte);
            this.att.AddAttendance(converted);
        }
        [HttpGet]
        [Route("Attendance/")]
        [Authorize(Roles = "Admin")]
        public List<AttendanceModel> Get()
        {
            List<AttendanceModel> atts = new List<AttendanceModel>();
            atts = this.att.GetById();
            return atts;
        }

    }
}
